//
//  PopupVC.m
//  testingPods
//
//  Created by Jose Catala on 04/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "PopupVC.h"

@interface PopupVC ()
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;

@property NSString *strTitle;
@property NSString *strMessage;

@end

@implementation PopupVC

#pragma mark inits

- (instancetype) initWithTitle:(NSString *)title andMessage:(NSString *)message
{
    UIStoryboard *st = [UIStoryboard storyboardWithName:@"Popups" bundle:nil];
    
    self = (PopupVC *)[st instantiateViewControllerWithIdentifier:@"PopupVC"];
    
    if (self)
    {
        self.strTitle = title;
        self.strMessage = message;
    }
    
    return self;
}

#pragma mark lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.lblMessage.text = _strMessage;
    self.lblTitle.text = _strTitle;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark actions

- (IBAction)btnOkDidTap:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
