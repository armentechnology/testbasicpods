//
//  PopupVC.h
//  testingPods
//
//  Created by Jose Catala on 04/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopupVC : UIViewController

- (instancetype) initWithTitle:(NSString *)title andMessage:(NSString *)message;

@end
