//
//  LoginVC.m
//  testingPods
//
//  Created by Jose Catala on 04/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "LoginVC.h"
#import "LoginTextFields.h"

@interface LoginVC ()

@property (weak, nonatomic) IBOutlet LoginTextFields *txtEmail;
@property (weak, nonatomic) IBOutlet LoginTextFields *txtPassword;

@end

@implementation LoginVC

#pragma mark lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

#pragma mark actions

- (IBAction)btnLoginDidTap:(id)sender
{
    if (_txtEmail.isValidData && _txtPassword.isValidData)
    {
        //Submit because password and email are valid
    }
    else
    {
        PopupVC* popup = [[PopupVC alloc]initWithTitle:@"Error" andMessage:@"Please, enter a valid username and password"];
        
        [self presentViewController:popup animated:NO completion:nil];
    }
}

@end
