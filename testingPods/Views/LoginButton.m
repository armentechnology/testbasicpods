//
//  LoginButton.m
//  testingPods
//
//  Created by Jose Catala on 04/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "LoginButton.h"
#import "UIColor+MTColors.h"

@implementation LoginButton


- (void)drawRect:(CGRect)rect
{
    self.layer.borderColor = [UIColor MTClearColor].CGColor;
    
    self.layer.cornerRadius = 5;
    
    self.layer.borderWidth = 1;
    
    self.layer.masksToBounds = YES;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor MTGreen];
    
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

@end
