//
//  LoginTextFields.h
//  testingPods
//
//  Created by Jose Catala on 04/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <UIKit/UIKit.h>


IB_DESIGNABLE
@interface LoginTextFields : UITextField

@property IBInspectable NSInteger maxLength;
@property IBInspectable BOOL isEmail;
@property IBInspectable BOOL isPassword;

@property BOOL isValidData;

@end
