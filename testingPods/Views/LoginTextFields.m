//
//  LoginTextFields.m
//  testingPods
//
//  Created by Jose Catala on 04/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "LoginTextFields.h"
#import "UIColor+MTColors.h"

@implementation LoginTextFields

- (void) dealloc
{
    [self removeTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

- (void)drawRect:(CGRect)rect
{
    self.layer.borderColor = [UIColor MTMidGrey].CGColor;
    
    self.layer.cornerRadius = 5;
    
    self.layer.borderWidth = 1;
    
    self.layer.masksToBounds = YES;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    [self addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

- (void) textFieldDidChange:(UITextField *)textField
{
    if (self.maxLength)
    {
        if (textField.text.length > self.maxLength)
        {
            textField.text = [textField.text substringWithRange:NSMakeRange(0, textField.text.length - 1)];
        }
    }
    
    if (_isEmail)
    {
        if ([self isValidEmail:textField.text])
        {
            UIImageView *iv = [[UIImageView alloc]initWithFrame:CGRectMake(5, 0, 30, self.frame.size.height)];
            iv.image = [UIImage imageNamed:@"tickBox"];
            [iv setContentMode:UIViewContentModeScaleAspectFit];
            self.leftViewMode = UITextFieldViewModeAlways;
            self.leftView = [self paddingViewWithImage:iv andPadding:10];
        }
        else
        {
            self.leftView = nil;
        }
    }
    else if (_isPassword)
    {
        if ([self isValidPassword:textField.text])
        {
            UIImageView *iv = [[UIImageView alloc]initWithFrame:CGRectMake(5, 0, 30, self.frame.size.height)];
            iv.image = [UIImage imageNamed:@"tickBox"];
            [iv setContentMode:UIViewContentModeScaleAspectFit];
            self.leftViewMode = UITextFieldViewModeAlways;
            self.leftView = [self paddingViewWithImage:iv andPadding:10];
        }
        else
        {
            self.leftView = nil;
        }
    }
}

- (BOOL)isValidEmail:(NSString *)email
{
    NSString *emailRegex =   @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSPredicate *emailResult = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    self.isValidData = [emailResult evaluateWithObject:email];
    
    return _isValidData;
}

- (BOOL)isValidPassword:(NSString *)password
{
    /*
     
     https://stackoverflow.com/questions/19605150/regex-for-password-must-contain-at-least-eight-characters-at-least-one-number-a
     
     ^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$
     
     This regex will enforce these rules:
     At least one upper case English letter, (?=.*?[A-Z])
     At least one lower case English letter, (?=.*?[a-z])
     At least one digit, (?=.*?[0-9])
     At least one special character, (?=.*?[#?!@$%^&*-])
     Minimum eight in length .{8,} (with the anchors)
     */
    
    NSString *pwdRegex =   @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
    
    NSPredicate *pwdResult = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pwdRegex];
    
    self.isValidData = [pwdResult evaluateWithObject:password];
    
    return _isValidData;
}

-(UIView*)paddingViewWithImage:(UIImageView*)imageView andPadding:(float)padding
{
    float height = CGRectGetHeight(imageView.frame);
    float width =  CGRectGetWidth(imageView.frame) + padding;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    
    [paddingView addSubview:imageView];
    
    return paddingView;
}

@end
