//
//  UIColor+MTColors.h
//  morethan
//
//  Created by MacMini on 10/04/2017.
//  Copyright © 2017 D3T. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (MTColors)
+ (UIColor *)MTBrandGreen;
+ (UIColor *)MTBrandGreenHalfAlpha;
+ (UIColor *)MTBrandGreenDark;
+ (UIColor *)MTBrandGreenDarkHalfAlpha;
+ (UIColor *)MTRed;
+ (UIColor *)MTAmber;
+ (UIColor *)MTGreen;
+ (UIColor *)MTLightGrey;
+ (UIColor *)MTMidGrey;
+ (UIColor *)MTDarkGrey;
+ (UIColor *)MTBlack;
+ (UIColor *)MTClearColor;
//+ (UIColor *)MTGreenArrowColor;
@end
