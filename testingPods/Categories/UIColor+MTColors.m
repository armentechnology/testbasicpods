//
//  UIColor+MTColors.m
//  morethan
//
//  Created by MacMini on 10/04/2017.
//  Copyright © 2017 D3T. All rights reserved.
//

#import "UIColor+MTColors.h"

@implementation UIColor (MTColors)

+ (UIColor *)MTBrandGreen
{
    return [UIColor colorWithRed:12.0f/255.0f green:132.0f/255.0f blue:0.0f/255.0f alpha:1];
}

+ (UIColor *)MTBrandGreenDark
{
    return [UIColor colorWithRed:1.0f/255.0f green:53.0f/255.0f blue:26.0f/255.0f alpha:1];
}

+ (UIColor *)MTBrandGreenHalfAlpha
{
    return [UIColor colorWithRed:12.0f/255.0f green:132.0f/255.0f blue:0.0f/255.0f alpha:0.5];
}

+ (UIColor *)MTBrandGreenDarkHalfAlpha
{
    return [UIColor colorWithRed:1.0f/255.0f green:53.0f/255.0f blue:26.0f/255.0f alpha:0.5];
}

+ (UIColor *)MTRed
{
    return [UIColor colorWithRed:221.0f/255.0f green:25.0f/255.0f blue:25.0f/255.0f alpha:1];
}

+ (UIColor *)MTAmber
{
    return [UIColor colorWithRed:253.0f/255.0f green:205.0f/255.0f blue:59.0f/255.0f alpha:1];
}

+ (UIColor *)MTGreen
{
    return [UIColor colorWithRed:12.0f/255.0f green:132.0f/255.0f blue:0.0f/255.0f alpha:1];
}

+ (UIColor *)MTLightGrey
{
    return [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1];
}

+ (UIColor *)MTMidGrey
{
    return [UIColor colorWithRed:221.0f/255.0f green:221.0f/255.0f blue:221.0f/255.0f alpha:1];
}

+ (UIColor *)MTDarkGrey
{
    return [UIColor colorWithRed:101.0f/255.0f green:101.0f/255.0f blue:101.0f/255.0f alpha:1];
}

+ (UIColor *)MTBlack
{
    return [UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1];
}

+ (UIColor *)MTClearColor
{
    return [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0];
}

//+ (UIColor *)MTGreenArrowColor
//{
//    return [UIColor colorWithRed:2.0f/255.0f green:131.0f/255.0f blue:1.0f/255.0f alpha:1];
//}
@end
